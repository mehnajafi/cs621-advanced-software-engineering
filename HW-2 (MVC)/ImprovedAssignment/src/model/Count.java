package model;

import java.util.ArrayList;

import view.Observer;

public class Count implements Stat{

	//keeps track of the total number of numbers the user has entered
	private int count;
	
	//the class that is observing count
	private Observer observer;

	public void register(Observer o) {
		observer = o;
	}

	public void unregister(Observer o) {
		observer = null;
		
	}

	public void nnotify() {
		ArrayList<String> temp = new ArrayList<String>();
		temp.add("count=" + String.valueOf(count));
		//System.out.print("update");
		observer.update(temp);
	}
	
	//sets the value of the count to the given input
	//@param: total number of numbers that the user has entered
	public void setCount(ArrayList<Integer> data){
		computeCount(data);
		nnotify();
	}
	
	//computes number of the given numbers
	public void computeCount(ArrayList<Integer> data){
		count = data.size();
	}
	
	//returns the count
	public int getCount(){
		return count;
	}
	
	//resets the value of the count to zero
	public void resetCount(){
		count = 0;
		nnotify();
	}

}
