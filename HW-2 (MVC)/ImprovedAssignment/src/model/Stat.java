package model;

import view.Observer;

//OBSERVER PATTERN (STAT, COUNT, MEAN, MEDIAN, VIEW.OBSERVER)
//Observable in Observer pattern
public interface Stat {
	
	//registers an observer
	public abstract void register(Observer o);
	
	//unregisters an observer
	public abstract void unregister(Observer o);
	
	//notifies observer(s) of the update
	//NOTE: changed name from notify to nnotify because of compilation error
	public abstract void nnotify();
	
}
