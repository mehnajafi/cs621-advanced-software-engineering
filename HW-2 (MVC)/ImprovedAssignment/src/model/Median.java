package model;

import static java.util.Collections.sort;

import java.util.ArrayList;

//import util.ListSort;
//import util.Sorter;
import view.Observer;

	
public class Median implements Stat{
	
	//current observer of the median
	private Observer observer;
	
	//current median
	private double median = 0;

	public void register(Observer o) {
		observer = o;
	}

	public void unregister(Observer o) {
		observer = null;
	}

	public void nnotify() {
		ArrayList<String> temp = new ArrayList<String>();
		temp.add("median=" + String.valueOf(median));
		observer.update(temp);
	}

	//sets the median of the given input
	public void setMedian(ArrayList<Integer> data) {
		computeMedian(data);
        nnotify();
	}
	
	//resets the median to zero
	public void resetMedian(){
		median = 0;
		nnotify();
	}
	
	//computes median of the given input
	public void computeMedian(ArrayList<Integer> data){
		if(data.size() != 0){
			sort(data);
			if (data.size()%2 == 0) {
				median = (data.get(data.size() >> 1) + data.get((data.size() >> 1) - 1)) / 2.;
			} else {
				median = data.get(data.size() >> 1);
			}
		}
		else{
			median = 0;
		}
	}
	
	//returns the current median
	public double getMedian(){
		return median;
	}
}
