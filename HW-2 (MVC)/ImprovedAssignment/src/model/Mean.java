package model;

import java.util.ArrayList;

import view.Observer;

public class Mean implements Stat{

	//observer of mean
	private Observer observer;
	
	//current mean
	private double mean;
	
	//current data (data that we are interested in having their mean)
	private ArrayList<Integer> data = new ArrayList<Integer>();
	
	public void register(Observer o) {
		observer = o;
	}

	public void unregister(Observer o) {
		observer = null;
	}

	public void nnotify() {
		ArrayList<String> temp = new ArrayList<String>();
		temp.add("mean=" + String.valueOf(mean));
		observer.update(temp);
		
		
		//ArrayList<String> temp2 = new ArrayList<String>();
		//if(data.size() > 0){
		//	temp2.add("number=" + String.valueOf(data.get(0)));
		//	for(int i = 1; i < data.size(); i++)
		//		temp2.add(String.valueOf(data.get(i)));
		//}
		//else{
		//	temp2.add("number=none");
		//}
		//observer.update(temp2);
	}

	//sets mean of the given input
	public void setMean(ArrayList<Integer> data){
        computeMean(data);
        nnotify();
	}

	//computes mean of the given input
	public void computeMean(ArrayList<Integer> data){
		this.data = data;
		if(data.size() != 0){
			double sum= 0;
			for (Integer value : data) {
				sum += value;
			}
			mean = sum / data.size();
		}
		else{
			mean = 0;
		}
	}
	
	//resets the mean
	public void resetMean(){
		mean = 0;
		data.clear();
		nnotify();
	}
	
	//returns current mean
	public double getMean(){
		return mean;
	}
}
