import static org.junit.Assert.*;

import java.util.ArrayList;

import model.Count;

import org.junit.Test;

//test cases for model element: count
public class ModelCountTest{
		   protected ArrayList<Integer> data = new ArrayList<Integer>();
		   protected Count counter = new Count();

		   @Test
		   public void testCount1(){
			  data.add(1);
			  
			  counter.computeCount(data);
			  double result = counter.getCount(); 

		      assertTrue(1 == result);
		   }
	
		   @Test
		   public void testCount0(){
			   counter.computeCount(data);
			   double result = counter.getCount();
			   
			   assertTrue(0 == result);
		   }
		   
		   @Test
		   public void testCountMany(){
			   data.add(1);
			   data.add(5);
			   data.add(10);
			   data.add(-1);
			   
			   counter.computeCount(data);
			   double result = counter.getCount();
			   
			   assertTrue(4 == result);
		   }
	
}
