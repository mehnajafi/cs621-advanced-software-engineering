package view;

import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JTextField;

//CompC in Composite design pattern
public class CountView implements View{
	//GUI elements of the view
	private JLabel countLabel = new JLabel("Numbers:");
	private final JTextField jtfCount = new JTextField(5);
     
	//current value of count
	private int count  = 0;
	
	public void update(ArrayList<String> data) {
		String value = data.get(0);
		
		if(value.startsWith("count=")){
			value = value.substring(6, value.length());
			count = Integer.parseInt(value);
		}
	}

	public void render() {
		jtfCount.setEditable(false);
		if(count != 0)
			jtfCount.setText("" + count);
		else
			jtfCount.setText("");
	}

	public JLabel getLabel(){
		return countLabel;
	}
	
	public JTextField getTextField(){
		return jtfCount;
	}

}
