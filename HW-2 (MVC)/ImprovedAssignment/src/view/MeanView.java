package view;

import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JTextField;

//CompA in Composite design pattern
public class MeanView implements View{
	//GUI elements of the view
	private final JTextField jtfMean = new JTextField(5);
	private final JLabel meanLabel =  new JLabel("Mean:");
	
	//value of current view
	private double mean = 0;
	
	public void update(ArrayList<String> data) {
		String value = data.get(0);
		
		if(value.startsWith("mean=")){
			value = value.substring(5, value.length());
			mean = Double.parseDouble(value);
		}
	}

	public void render() {
		jtfMean.setEditable(false);
		if(mean != 0)
			jtfMean.setText("" + mean);
		else
			jtfMean.setText("");
	}

	public JLabel getLabel() {
		return meanLabel;
	}

	public JTextField getTextField() {
		return jtfMean;
	}

}
