package view;

import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JTextField;

//CompB in Composite design pattern
public class MedianView implements View{
	//GUI elements for median view
	private JLabel medianLabel = new JLabel("Median:");
	private final JTextField jtfMedian = new JTextField(5);
	
	//value of current median
	private double median = 0;
	
	public void update(ArrayList<String> data) {
		String value = data.get(0);
		
		if(value.startsWith("median=")){
			value = value.substring(7, value.length());
			median = Double.parseDouble(value);
		}
	}

	public void render() {
		jtfMedian.setEditable(false);
		if(median != 0)
			jtfMedian.setText("" + median);
		else
			jtfMedian.setText("");
	}

	public JLabel getLabel() {
		return medianLabel;
	}

	public JTextField getTextField() {
		return jtfMedian;
	}

}
