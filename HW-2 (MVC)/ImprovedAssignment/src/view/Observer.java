package view;

import java.util.ArrayList;

//Observer in Observer pattern
public interface Observer {
	public abstract void update(ArrayList<String> data);
}
