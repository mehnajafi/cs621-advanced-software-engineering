package view;

import javax.swing.JLabel;
import javax.swing.JTextField;

//COMPOSITE DESIGN PATTERN(VIEW, COMPLEX VIEW, COUNT VIEW, MEAN VIEW, MEDIAN VIEW)
//ASSUMPTION: every view has at most one label, one text field
public interface View extends Observer{
	
	//renders the view
	public abstract void render();
	
	//returns label of the view
	public abstract JLabel getLabel();
	
	//returns text field of the view
	public abstract JTextField getTextField(); 
	
}
