package view;

import java.awt.FlowLayout;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

//the view that contains all other views (Composite in COMPOSITE DESIGN PATTERN)
public class ComplexView implements View{
	//GUI elements of the view
	private JPanel jpStats = new JPanel(new FlowLayout(FlowLayout.CENTER));

	//list of views that the complex view contains
	private ArrayList<View> views = new ArrayList<View>();
	
	
	public void update(ArrayList<String> data) {
		for(View view: views){
			view.update(data);
		}
		render();
	}

	public void render() {
		for(View view: views){
			view.render();
			if(view.getLabel() != null)
				jpStats.add(view.getLabel());
			if(view.getTextField() != null)
				jpStats.add(view.getTextField());
		}
	}
	
	public void addView(View v){
		views.add(v);
	}
	
	public void removeView(View v){
		for(View vi: views)
			if(vi.equals(v))
				views.remove(v);
	}

	public JLabel getLabel() {
		return null;
	}

	public JTextField getTextField() {
		return null;
	}
	
	public JPanel getPanel(){
		return jpStats;
	}

}
