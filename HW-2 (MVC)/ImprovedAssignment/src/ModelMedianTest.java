import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import model.Median;

import org.junit.Test;

//test cases for model element: median
public class ModelMedianTest{
	   protected ArrayList<Integer> data = new ArrayList<Integer>();
	   protected Median medianner = new Median();
	   private static double EPS = 1e-9;

	   @Test
	   public void testMedian1(){
		  data.add(10);
		  
		  medianner.computeMedian(data);
		  double result = medianner.getMedian(); 

		  assertEquals(10, result, EPS);
	   }

	   @Test
	   public void testMedian0(){
		   medianner.computeMedian(data);
		   double result = medianner.getMedian();
		   
		   assertEquals(0, result, EPS);
	   }
	   
	   @Test
	   public void testMeanMany(){
		   data.add(10);
		   data.add(400);
		   data.add(50);
		   
		   medianner.computeMedian(data);
		   double result = medianner.getMedian();
		   
		   assertTrue(50 == result);
	   }
}

