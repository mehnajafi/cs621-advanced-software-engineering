package controller;


//DECORATOR PATTERN (INPUTCONTROLLER, FILEINPUTCONTROLLER, USERINPUTCONTROLLER, BUFFEREDINPUTCONTROLLER)
//ASSUMPTION: NO HYBRID INPUT IS CONSIDERED 
public interface InputController {
	
	//obtains numbers from a source of information (it could be file or user or any other source)
	public abstract void read();
}
