package controller;


import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import model.Count;
import model.Mean;
import model.Median;
import view.Observer;


public class UserInputController implements InputController{
	public static ArrayList<Integer> data = new ArrayList<Integer>();
	
	//all needed GUI components for getting input from the user (numbers, reset)
	private final static JTextField jtfNumber = new JTextField(5);
	private static JButton jbAdd = new JButton("Add number");
	private static JButton jbReset = new JButton("Reset");
	private final static JTextArea jtaNumbers = new JTextArea(10,50);
	private static JPanel jpInput = new JPanel(new FlowLayout(FlowLayout.CENTER));
	
	//model elements
	private Count counter = new Count();
	private Mean meanner  = new Mean();
	private Median medianer = new Median();
	
	//gets input from the user (either numbers (Add Number) or Reset)
	public void read() {
		// Create the main frame of the application, and set size and position
		//JFrame jfMain = new JFrame("Simple stats");
		//jfMain.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
	    //jfMain.setSize(600,400);
	    //jfMain.setLocationRelativeTo(null);
				
				
		//JPanel jpStats = new JPanel(new FlowLayout(FlowLayout.CENTER));
		//jfMain.getContentPane().add(jpStats, BorderLayout.CENTER);
				
				
		// TextArea that shows all the numbers
		//final JTextArea jtaNumbers = new JTextArea(10,50);
		jtaNumbers.setEditable(false);
		//jfMain.getContentPane().add(jtaNumbers, BorderLayout.SOUTH);
		        
		        
		 // Panel with a text field/button to enter numbers and a button to reset the application
		 //JButton jbReset = new JButton("Reset");
		 jbReset.addActionListener(new ActionListener() {
		 // The interface ActionListener defines a call-back method actionPerformed,
		 // which is invoked if the user interacts with the GUI component -- in this
		 // case, if the user clicks the button.
		     public void actionPerformed(ActionEvent e) {
		           // Clear the ArrayList and all text fields
		           data.clear();
		           
		           jtaNumbers.setText("");
		           jtfNumber.setText("");
		           
		           counter.resetCount();
		           meanner.resetMean();
		           medianer.resetMedian();
		           }
		      });
		        
		        
		   //final JTextField jtfNumber = new JTextField(5);
		   //JButton jbAdd = new JButton("Add number");
		   jbAdd.addActionListener(new ActionListener() {
		       public void actionPerformed(ActionEvent e) {
		             // Parse input and add number to the ArrayList
		             //
		             // What if the user doesn't enter a number...?
		             // Input validation and error handling is important -- you can omit it
		             // for this homework, though.
		             Integer num = Integer.parseInt(jtfNumber.getText());
		             data.add(num);
		             jtaNumbers.append(num + ",");
		             
		             //manipulates Model elements
		             counter.setCount(data);
		             meanner.setMean(data);
		             medianer.setMedian(data);
		            }
		    });
		        
		    //JPanel jpInput = new JPanel(new FlowLayout(FlowLayout.CENTER));
		    //jpInput.add(jtfNumber);
		    //jpInput.add(jbAdd);
		    //jpInput.add(jbReset);
		    //jfMain.getContentPane().add(jpInput, BorderLayout.NORTH);
		 
		    // Show the frame
		    //jfMain.setVisible(true);
	}
	
	
	//returns numbers text area
	public static JTextArea getNumbersTextArea(){
		return jtaNumbers;
	}
	
	//returns Add button
	public JButton getAddButton(){
		return jbAdd;
	}
	
	//returns Reset button
	public JButton getResetButton(){
		return jbReset;
	}
	
	//returns panel
	public static JPanel getPanel(){
	    jpInput.add(jtfNumber);
	    jpInput.add(jbAdd);
	    jpInput.add(jbReset);
		return jpInput;
	}
	
	//sets observers of model elements
	public void setObserver(Observer o){
		counter.register(o);
		meanner.register(o);
		medianer.register(o);
	}
}
