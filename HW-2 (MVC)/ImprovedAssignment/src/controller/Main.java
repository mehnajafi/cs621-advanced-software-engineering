package controller;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import view.ComplexView;
import view.CountView;
import view.MeanView;
import view.MedianView;

//core of the controller in MVC architectural pattern
public class Main {

	public static void main(String ... args) {
		// Create the main frame of the application, and set size and position
		JFrame jfMain = new JFrame("Simple stats");
		jfMain.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
	    jfMain.setSize(600,400);
	    jfMain.setLocationRelativeTo(null);
		
		UserInputController userInput = new UserInputController();
		
	    //creates complex view (count, mean, median)
	    ComplexView comView = new ComplexView();
	    CountView countView = new CountView();
	    MeanView meanView   = new MeanView();
	    MedianView medianView = new MedianView();
	    comView.addView(countView);
	    comView.addView(medianView);
	    comView.addView(meanView);
	    comView.render();
	    
	    //sets the observer of model elements which is complex view
	    userInput.setObserver(comView);
	    
	    //gets input from user
	    userInput.read();
	    
		//JPanel jpStats = new JPanel(new FlowLayout(FlowLayout.CENTER));
		jfMain.getContentPane().add(comView.getPanel(), BorderLayout.CENTER);
		
		jfMain.getContentPane().add(UserInputController.getNumbersTextArea(), BorderLayout.SOUTH);
		
		//JPanel jpInput = new JPanel(new FlowLayout(FlowLayout.CENTER));
		
		//jpInput.add(userInput.getAddButton());
		//jpInput.add(userInput.getResetButton());
		jfMain.getContentPane().add(UserInputController.getPanel(), BorderLayout.NORTH);
		//userInput.getPanel().add(comView.getTextArea());
		// Show the frame
		jfMain.setVisible(true);
	}
}
