import java.util.ArrayList;
import static org.junit.Assert.*;

import org.junit.Test;

import model.Mean;

//test cases for model element: mean
public class ModelMeanTest{
	   protected ArrayList<Integer> data = new ArrayList<Integer>();
	   protected Mean meanner = new Mean();
	   private static double EPS = 1e-9;

	   @Test
	   public void testMean1(){
		  data.add(10);
		  
		  meanner.computeMean(data);
		  double result = meanner.getMean(); 

		  assertEquals(10, result, EPS);
	   }

	   @Test
	   public void testMean0(){
		   meanner.computeMean(data);
		   double result = meanner.getMean();
		   
		   assertEquals(0, result, EPS);
	   }
	   
	   @Test
	   public void testMeanMany(){
		   data.add(10);
		   data.add(50);
		   
		   meanner.computeMean(data);
		   double result = meanner.getMean();
		   
		   assertTrue(30 == result);
	   }
}
