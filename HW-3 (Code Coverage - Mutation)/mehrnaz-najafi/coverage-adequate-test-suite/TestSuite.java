package triangle.test;

import triangle.Triangle;
import triangle.Triangle.Type;
import junit.framework.TestCase;
import static triangle.Triangle.Type.*;

public class TestSuite extends TestCase {

   public void test1() {
       assertEquals (triangle.Triangle.classify(0,0,0), INVALID);
   }
   
   public void test2(){
	   assertEquals(triangle.Triangle.classify(1,1,1), EQUILATERAL);
   }
   
   public void test3(){
	   assertEquals(triangle.Triangle.classify(3,4,5), SCALENE);
   }
   
   public void test4(){
	   assertEquals(triangle.Triangle.classify(1,2,5), INVALID);
   }

   public void test5(){
	  assertEquals(triangle.Triangle.classify(1,1,3), INVALID);
   }	
 
   public void test6(){
	  assertEquals(triangle.Triangle.classify(2,2,3), ISOSCELES);
   }

   public void test7(){
	  assertEquals(triangle.Triangle.classify(2,3,2), ISOSCELES);
   }

   public void test8(){
	  assertEquals(triangle.Triangle.classify(3,2,2), ISOSCELES);
   }

   public void test9(){
	  Triangle t = new Triangle();
   }

   public void test10(){
          assertEquals(triangle.Triangle.classify(3,-2,2), INVALID);
   }

   public void test11(){
          assertEquals(triangle.Triangle.classify(3,2,-2), INVALID);
   }

   public void test12(){
 	  assertEquals(triangle.Triangle.classify(4,2,3), SCALENE);
   }

   public void test13(){
 	  assertEquals(triangle.Triangle.classify(1,4,3), INVALID);
   }

   public void test14(){
 	  assertEquals(triangle.Triangle.classify(4,1,3), INVALID);
   }

   public void test15(){
 	  assertEquals(triangle.Triangle.classify(2,1,2), ISOSCELES);
   }

   public void test16(){
 	  assertEquals(triangle.Triangle.classify(1,2,2), ISOSCELES);
   }

   public void test17(){
 	  assertEquals(triangle.Triangle.classify(2,5,2), INVALID);
   }

   public void test18(){
 	  assertEquals(triangle.Triangle.classify(5,2,2), INVALID);
   }

}


