
# coding: utf-8

# In[2]:

import numpy as np
import pylab as pl
import os
#return give packages sizes by cloc
def getPackageSize(cloc_folder, project_src, package_name):
    #cloc = '/Users/anahita/Desktop/results3'
    cloc = cloc_folder
    os.chdir(cloc)
    
    #package size by cloc
    size = []
    for i in range(len(package_name)):
        #filename = '/Users/anahita/Desktop/folder/poi/src/java'
        print(package_name[i])
        filename = project_src
        text = package_name[i].split('.')
        for j in range(len(text)):
            filename = filename + '/' + text[j]
        #print("filename" + filename)
        #print("package" + package_name[i] + str(i))
        os.system('cloc ' + filename + ' .> result' + str(i) + '.txt')
        
        file = open(cloc + '/result' + str(i) + '.txt', 'r')
        file_content = file.readlines()
        #print(file_content)
        row = []
        for pointer in range(len(file_content)):
            #print(file_content[pointer])
            if('Java' in file_content[pointer]):
                row = file_content[pointer]
        row_content = row.split(' ')
        number = row_content[len(row_content) - 1]
        temp = number[0: len(number) - 1]
        #print("tempvalue= " + temp)
        size.append(float(temp))
    #size has been associated to package_name
    return size


# In[ ]:




# In[ ]:




# In[ ]:




# In[3]:

import os
#embeds grep command
def getGrep(word, package, project_src):
    filename = project_src
    text = package.split('.')
    for j in range(len(text)):
        filename = filename + '/' + text[j]
    #print("filename" + filename)
    #print("package" + package)
    #os.chdir(filename)
    os.system('grep -r \'' + word + '\' ' + filename + ' | wc -l >> count' + package + '.txt')
    #print("hi")
    file = open('count' + package + '.txt', 'r')
    file_content = file.readlines()
    
    num = 0
    for line in file_content:
        temp = line.strip()
        num = num + float(temp)
    print(num)
    return num


# In[4]:

import numpy as np
import pylab as pl
import os
#NOTE: my little correction (CLOVER)
#just put all directories under src directory
#also, remove all other types of files (only .java)
#'/Users/anahita/Desktop/closure.txt'
def readCloverPoiPackageLevel(apache_clover_file):
    #closure.pdf has been converted to closure.txt
    content = []
    with open(apache_clover_file) as f:
        content = f.readlines()
    
    #read statement coverage of each file
    stmt_coverage = []
    package_name = []
    for i in range(len(content)):
        if(content[i].startswith('org.apache')):
            temp = content[i]
            #print("temp" + temp)
            
            pos = temp.index(' ')
            name = temp[0:pos]
            package_name.append(temp[0:pos])
            #print(name)
            
            temp = temp[pos + 1: ]
            pos = temp.index(' ')
            temp = temp[pos + 1: ]
            per = temp.index('%')
            coverage = temp[0:per]
            stmt_coverage.append(coverage)
    
    #correction - 
    #print(stmt_coverage)
    #stmt_coverage[len(stmt_coverage)-1] = '100'
    #for i in range(len(stmt_coverage)):
    #    stmt_coverage[i] = float(stmt_coverage[i])
    
    #size = getPackageSize('/Users/anahita/Desktop/results', '/Users/anahita/Desktop/untitled/closure-compiler-master\ 4/src', package_name)
    #All statement coverage for each package has been stored in stmt_package
    #cloc = 
    #os.chdir(cloc)
    
    #package size by cloc
    #size = []
    #for i in range(len(package_name)):
        #filename = '/Users/anahita/Desktop/folder/closure-compiler-master\ 4/src'
        #text = package_name[i].split('.')
        #for j in range(len(text)):
            #filename = filename + '/' + text[j]
        #print("filename" + filename)
        #print("package" + package_name[i])
        #os.system('cloc ' + filename + ' . > result' + str(i) + '.txt')
        
        #file = open(cloc + '/result' + str(i) + '.txt', 'r')
        #file_content = file.readlines()
        #print(file_content)
        #row = []
        #for pointer in range(len(file_content)):
            #print(file_content[pointer])
            #if('Java' in file_content[pointer]):
                #row = file_content[pointer]
        #row_content = row.split(' ')
        #number = row_content[len(row_content) - 1]
        #temp = number[0: len(number) - 1]
        #print("tempvalue= " + temp)
        #size.append(float(temp))
    
    #print(package_name)
    #pl.xticks(x, package_name)
    #pl.xticks(range(length), package_name) 
    #please note that they are sorted by statement coverage value
    #pl.plot(stmt_coverage, size, '.')
    #pl.savefig('/Users/anahita/Desktop/closure-package-stmtcoverage-viceversa.pdf')
    #pl.show()
    result = []
    for i in range(len(package_name)):
        p = package_name[i]
        s = stmt_coverage[i]
        #si = size[i]
        result.append(str(p) + "*" + str(s))
    return result

#result = readCloverClosurePackageLevel()
#for i in range(len(result)):
#    print(result[i])


# In[5]:

import csv
#file_name = '/Users/anahita/Desktop/report.csv'
def readJacocoPackageLevel(file_name):
    package_name = []
    with open(file_name, 'rt') as csvfile:
        first = True
        first_row = True
        current_package = " "
        total_lines = 0
        covered_line = 0
        stmt_coverage = []
        for line in csvfile.readlines():
            array = line.split(',')
            if(first_row != True):
                if(first == True):
                    package_name.append(array[1])
                    first = False
                    current_package = array[1]
                    total_lines = float(array[7]) + float(array[8])
                    covered_line = float(array[8])
                else:
                    if(array[1] != current_package):
                        stmt_coverage.append(covered_line / total_lines)
                        package_name.append(array[1])
                        current_package = array[1]
                        covered_line = float(array[8])
                        total_lines = float(array[7]) + float(array[8])
                    else:
                        total_lines = total_lines + float(array[7]) + float(array[8])
                        covered_line = covered_line + float(array[8])
            first_row = False
        stmt_coverage.append(covered_line / total_lines)
    
    result = []
    for i in range(len(package_name)):
        s = stmt_coverage[i]
        p = package_name[i]
        result.append(str(p) + "*" + str(s))
    return result

#result = readJacocoPackageLevel('/Users/anahita/Desktop/report.csv')
#for i in range(len(result)):
#    print(result[i])


# In[6]:

import codecs
def readJMockitPackageLevel(file_name, package_prefix):
    #file name must be changed
    f=codecs.open(file_name, 'r')
    package_name = []
    stmt_coverage = []
    for line in f.readlines():
        #print(line)
        if(('package\'>' in line) or ('package click' in line)):
            #print("pref= " + package_prefix)
            index = line.index(package_prefix)
            temp = line[index: ]
            index2 = temp.index('<')
            package_name.append(temp[:index2])
            #print(temp[:index2])
            
        if('class=\'pt\'' in line):
            #print(line)
            index = line.index('>')
            temp = line[index: ]
            #print(temp)
            index2 = temp.index('<')
            num = temp[1: index2 - 1]
            stmt_coverage.append(num)
            #print(num)
            
    result = []
    for i in range(len(package_name)):
        p = package_name[i]
        s = stmt_coverage[i]
        result.append(str(p) + "*" + str(s))
    return result

#result = readJMockitPackageLevel("/Users/anahita/Desktop/index2.html", "com.google")
#for i in range(len(result)):
#    print(result[i])


# In[ ]:




# In[ ]:




# In[16]:

import numpy as np
import scipy.stats as st
#compues correlation between keyword and variance of statement coverage
def computeWordPoi(poi_packages, clover_report_file, jacoco_report_file, jmockit_report_file):
    result1 = readCloverPoiPackageLevel(clover_report_file)
    result2 = readJacocoPackageLevel(jacoco_report_file)
    result3 = readJMockitPackageLevel(jmockit_report_file, 'org.apache')
    
    #STEP1: GET PACKAGE NAMES
    file = open(poi_packages)
    package_name = []
    lines = file.readlines()
    counter = 0
    for package in lines:
        if(counter != (len(lines) - 1)):
            package_name.append(package[:len(package) - 1])
        else:
            package_name.append(package[:len(package)])
        counter = counter + 1
    #print("lines=" + str(len(lines)))
    #STEP2: GET PACKAGE SIZES
    #size = getPackageSize('/Users/anahita/Desktop/results3', '/Users/anahita/Desktop/folder/poi/src/java', package_name)

    #STEP3: GET STATEMENT COVERAGE NUMBERS
    clover = []
    for i in range(len(package_name)):
        clover.append(0)
        
    counter = 0
    for package in package_name:
        for item in result1:
            index = item.index('*')
            p_name = item[:index]
            if(package == p_name):
                clover[counter] = float(item[index + 1:])
        counter = counter + 1
    
    jac = []
    for i in range(len(package_name)):
        jac.append(0)
        
    counter = 0
    for package in package_name:
        for item in result2:
            index = item.index('*')
            p_name = item[:index]
            if(package == p_name):
                jac[counter] = float(item[index + 1:]) * 100
        counter = counter + 1
        
    jmo = []
    for i in range(len(package_name)):
        jmo.append(0)
        
    counter = 0
    for package in package_name:
        for item in result3:
            index = item.index('*')
            p_name = item[:index]
            if(package == p_name):
                jmo[counter] = float(item[index + 1:])
        counter = counter + 1
    
    #STEP4: GET NUMBER OF ASSERTS
    statics = []
    for package in package_name:
        num = getGrep('public class', package, '/Users/anahita/Desktop/untitled/poi/src/java')
        num2 = getGrep('public class', package, '/Users/anahita/Desktop/untitled/poi/src/java')
        #num2 = getAssert(package, '/Users/anahita/Desktop/untitled/closure-compiler-master\ 4/src')
        statics.append(float(num) + float(num2))
        
    #print(interfaces)
    #STEP5: CALCULATE VARIANCE OF DATA
    varm = []
    for i in range(len(package_name)):
        vart = []
        vart.append(clover[i])
        vart.append(jac[i])
        vart.append(jmo[i])
        a = np.asarray(vart)
        varm.append(np.var(a))
        #print("package= " + package_name[i] + "*" + str(clover[i]) + "*" + str(jac[i]) + "*" + str(jmo[i]))
        #print("var= " + str(varm[i]))
        #print("interface= " + str(interfaces[i]))
    
    #STEP6: DRAW GRAPH VARIANCE VS. ASSERTS
    #pl.plot(new_asserts, new_vars, '.')
    #pl.savefig('/Users/anahita/Desktop/closure-asserts-var-2.pdf')
    #pl.show()
    #print(statics)
    #print(varm)
    kel = st.kendalltau(statics[1:], varm[1:])  
    kel3 = np.corrcoef(statics[1:], varm[1:])
    return kel
#computeInterfaceMath('/Users/anahita/Desktop/mathpackages.txt')
kel3 = computeWordPoi('/Users/anahita/Desktop/poipackages.txt', '/Users/anahita/Desktop/poi.txt', '/Users/anahita/Desktop/report.csv', '/Users/anahita/Desktop/index.html')
print(kel3)


# In[11]:

import numpy as np
import scipy.stats as st
#computes correlation between size of program and variance of statement coverage
def computeSizeCorrPoi(poi_packages, clover_report_file, jacoco_report_file, jmockit_report_file):
    result1 = readCloverPoiPackageLevel(clover_report_file)
    result2 = readJacocoPackageLevel(jacoco_report_file)
    result3 = readJMockitPackageLevel(jmockit_report_file, 'org.apache')
    
    #STEP1: GET PACKAGE NAMES
    file = open(poi_packages)
    package_name = []
    lines = file.readlines()
    counter = 0
    for package in lines:
        if(counter != (len(lines) - 1)):
            package_name.append(package[:len(package) - 1])
        else:
            package_name.append(package[:len(package)])
        counter = counter + 1
    #print("lines=" + str(len(lines)))
    #STEP2: GET PACKAGE SIZES
    size = getPackageSize('/Users/anahita/Desktop/results3', '/Users/anahita/Desktop/untitled/poi/src/java', package_name)

    #STEP3: GET STATEMENT COVERAGE NUMBERS
    clover = []
    for i in range(len(package_name)):
        clover.append(0)
        
    counter = 0
    for package in package_name:
        for item in result1:
            index = item.index('*')
            p_name = item[:index]
            if(package == p_name):
                clover[counter] = float(item[index + 1:])
        counter = counter + 1
    
    jac = []
    for i in range(len(package_name)):
        jac.append(0)
        
    counter = 0
    for package in package_name:
        for item in result2:
            index = item.index('*')
            p_name = item[:index]
            if(package == p_name):
                jac[counter] = float(item[index + 1:]) * 100
        counter = counter + 1
        
    jmo = []
    for i in range(len(package_name)):
        jmo.append(0)
        
    counter = 0
    for package in package_name:
        for item in result3:
            index = item.index('*')
            p_name = item[:index]
            if(package == p_name):
                jmo[counter] = float(item[index + 1:])
        counter = counter + 1
    
    #STEP4: GET NUMBER OF ASSERTS
    #statics = []
    #for package in package_name:
        #num = getStatic(package, '/Users/anahita/Desktop/untitled/poi/src/java')
        #num2 = getAssert(package, '/Users/anahita/Desktop/untitled/closure-compiler-master\ 4/src')
        #statics.append(float(num))
        
    #print(interfaces)
    #STEP5: CALCULATE VARIANCE OF DATA
    varm = []
    for i in range(len(package_name)):
        vart = []
        vart.append(clover[i])
        vart.append(jac[i])
        vart.append(jmo[i])
        a = np.asarray(vart)
        varm.append(np.var(a))
        #print("package= " + package_name[i] + "*" + str(clover[i]) + "*" + str(jac[i]) + "*" + str(jmo[i]))
        #print("var= " + str(varm[i]))
        #print("interface= " + str(interfaces[i]))
    
    #STEP6: DRAW GRAPH VARIANCE VS. ASSERTS
    #pl.plot(size, varm, '.')
    #pl.xlabel('Package Size')
    #pl.ylabel('Variance of Statement Coverage')
    #pl.savefig('/Users/anahita/Desktop/closure-asserts-var-2.pdf')
    #pl.show()
    #print(statics)
    #print(varm)
    
    ff = open('/Users/anahita/Desktop/poipoi.txt', 'w')
    for m in range(len(package_name)):
        ff.write(str(size[m]) + "\n")
        ff.write(str(varm[m]) + "\n")
        
    kel = st.kendalltau(size, varm)  
    kel3 = np.corrcoef(size[1:], varm[1:])
    return kel3
#computeInterfaceMath('/Users/anahita/Desktop/mathpackages.txt')
kel3 = computeSizeCorrPoi('/Users/anahita/Desktop/poipackages.txt', '/Users/anahita/Desktop/poi.txt', '/Users/anahita/Desktop/report.csv', '/Users/anahita/Desktop/index.html')
print(kel3)


# In[12]:

import numpy as np
import matplotlib.pyplot as pl
#'/Users/anahita/Desktop/closurepackages.txt'
#'/Users/anahita/Desktop/report.csv'
#'/Users/anahita/Desktop/index.html'
#draws graphs
def drawGraphPackageCoverage(poi_packages, clover_report_file, jacoco_report_file, jmockit_report_file):
    result1 = readCloverPoiPackageLevel(clover_report_file)
    result2 = readJacocoPackageLevel(jacoco_report_file)
    result3 = readJMockitPackageLevel(jmockit_report_file, 'org.apache')
    
    #STEP1: GET PACKAGE NAMES
    file = open(poi_packages)
    package_name = []
    lines = file.readlines()
    counter = 0
    for package in lines:
        if(counter != (len(lines) - 1)):
            package_name.append(package[:len(package) - 1])
        else:
            package_name.append(package[:len(package)])
        counter = counter + 1
    
    print("lines" + str(len(lines)))
    #STEP2: GET STATEMENT COVERAGE NUMBERS
    clover = []
    for i in range(len(package_name)):
        clover.append(0)
        
    counter = 0
    for package in package_name:
        for item in result1:
            index = item.index('*')
            p_name = item[:index]
            if(package == p_name):
                clover[counter] = float(item[index + 1:])
        counter = counter + 1
    
    jac = []
    for i in range(len(package_name)):
        jac.append(0)
        
    counter = 0
    for package in package_name:
        for item in result2:
            index = item.index('*')
            p_name = item[:index]
            if(package == p_name):
                jac[counter] = float(item[index + 1:]) * 100
        counter = counter + 1
        
    jmo = []
    for i in range(len(package_name)):
        jmo.append(0)
        
    counter = 0
    for package in package_name:
        for item in result3:
            index = item.index('*')
            p_name = item[:index]
            if(package == p_name):
                jmo[counter] = float(item[index + 1:])
        counter = counter + 1

    #STEP3: PACKAGE NUMBERS
    x = []
    for i in range(len(package_name)):
        x.append(i)
        
    #STEP4: DRAW GRAPH PACKAGE VS. COVERAGE
    pl.plot(x, clover, '.')
    pl.plot(x, jac, '*')
    pl.plot(x, jmo, '+')
    pl.xlabel('Package No.')
    pl.ylabel('Statement Coverage')
    pl.savefig('/Users/anahita/Desktop/poi-package-statementcoverage.pdf')
    pl.show()
    
#drawGraphPackageCoverage('/Users/anahita/Desktop/poipackages.txt', '/Users/anahita/Desktop/poi.txt', '/Users/anahita/Desktop/report.csv', '/Users/anahita/Desktop/index.html')


# In[ ]:

import numpy as np
import matplotlib.pyplot as pl
#draws graph varaiance of packages
def drawGraphPackageVariance(poi_packages, clover_report_file, jacoco_report_file, jmockit_report_file):
    result1 = readCloverPoiPackageLevel(clover_report_file)
    result2 = readJacocoPackageLevel(jacoco_report_file)
    result3 = readJMockitPackageLevel(jmockit_report_file, 'org.apache')
    
    #STEP1: GET PACKAGE NAMES
    file = open(poi_packages)
    package_name = []
    lines = file.readlines()
    counter = 0
    for package in lines:
        if(counter != (len(lines) - 1)):
            package_name.append(package[:len(package) - 1])
        else:
            package_name.append(package[:len(package)])
        counter = counter + 1
    #print("lines=" + str(len(lines)))
    #STEP2: GET PACKAGE SIZES
    #size = getPackageSize('/Users/anahita/Desktop/results2', '/Users/anahita/Desktop/untitled/commons-math-2/src/main/java', package_name)

    #STEP3: GET STATEMENT COVERAGE NUMBERS
    clover = []
    for i in range(len(package_name)):
        clover.append(0)
        
    counter = 0
    for package in package_name:
        for item in result1:
            index = item.index('*')
            p_name = item[:index]
            if(package == p_name):
                clover[counter] = float(item[index + 1:])
        counter = counter + 1
    
    jac = []
    for i in range(len(package_name)):
        jac.append(0)
        
    counter = 0
    for package in package_name:
        for item in result2:
            index = item.index('*')
            p_name = item[:index]
            if(package == p_name):
                jac[counter] = float(item[index + 1:]) * 100
        counter = counter + 1
        
    jmo = []
    for i in range(len(package_name)):
        jmo.append(0)
        
    counter = 0
    for package in package_name:
        for item in result3:
            index = item.index('*')
            p_name = item[:index]
            if(package == p_name):
                jmo[counter] = float(item[index + 1:])
        counter = counter + 1
    
    #STEP4: GET NUMBER OF ASSERTS
    #asserts = []
    #for package in package_name:
        #num = getAssert(package, '/Users/anahita/Desktop/untitled/commons-math-2/src/main/java')
        #num2 = getAssert(package, '/Users/anahita/Desktop/untitled/closure-compiler-master\ 4/src')
        #asserts.append(float(num))
        
    #print(asserts)
    #STEP5: CALCULATE VARIANCE OF DATA
    varm = []
    for i in range(len(package_name)):
        vart = []
        vart.append(clover[i])
        vart.append(jac[i])
        vart.append(jmo[i])
        a = np.asarray(vart)
        varm.append(np.var(a))
        #print("package= " + package_name[i] + "*" + str(clover[i]) + "*" + str(jac[i]) + "*" + str(jmo[i]))
        #print("var= " + str(varm[i]))
        #print("asserts= " + str(asserts[i]))
    
    x = []
    for i in range(len(package_name)):
        x.append(i)
        
    #STEP6: DRAW GRAPH PACKAGE VS. VARIANCE
    pl.plot(x, varm, '.')
    pl.xlabel("Package No.")
    pl.ylabel("Variance of Statement Coverage")
    pl.savefig('/Users/anahita/Desktop/poi-package-variance.pdf')
    pl.show()
    
#computeAssertMath('/Users/anahita/Desktop/mathpackages.txt')
drawGraphPackageVariance('/Users/anahita/Desktop/poipackages.txt', '/Users/anahita/Desktop/poi.txt', '/Users/anahita/Desktop/report.csv', '/Users/anahita/Desktop/index.html')


# In[ ]:



