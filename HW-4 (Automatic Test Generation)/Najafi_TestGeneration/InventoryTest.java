package edu.ncsu.csc326.coffeemaker;

import junit.framework.TestCase;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;
import edu.ncsu.csc326.coffeemaker.Inventory;
import edu.ncsu.csc326.coffeemaker.exceptions.InventoryException;
import edu.ncsu.csc326.coffeemaker.exceptions.RecipeException;

public class InventoryTest extends TestCase{
	
	
	@Test
	public void testDefaults() {
		Inventory inventory = new Inventory();
		int chocUnits = inventory.getChocolate();
		assertEquals(15, chocUnits);
	}
	
	public void testAddChocolate1() throws InventoryException{
		Inventory inventory = new Inventory();
		inventory.addChocolate("10");
		int chocUnits = inventory.getChocolate();
		assertEquals(25, chocUnits);
	}
	
	public void testAddChocolate2() throws InventoryException{
		Inventory inventory = new Inventory();
		inventory.addChocolate("0");
		int chocUnits = inventory.getChocolate();
		assertEquals(15, chocUnits);
	}
	
	public void testAddChocolate3(){
		Inventory inventory = new Inventory();
		try {
			inventory.addChocolate("-10");
		} catch (InventoryException e) {
			assertEquals("Units of chocolate must be a positive integer", e.getMessage());
		}
	}
	
	public void testAddChocolate4(){
		Inventory inventory = new Inventory();
		try {
			inventory.addChocolate("salam");
		} catch (InventoryException e) {
			assertEquals("Units of chocolate must be a positive integer", e.getMessage());
		}
	}
	
	
	public void testAddCoffee1() throws InventoryException{
		Inventory inventory = new Inventory();
		inventory.addCoffee("10");
		int coffee = inventory.getCoffee();
		assertEquals(25, coffee);
	}
	
	public void testAddCoffee2() throws InventoryException{
		Inventory inventory = new Inventory();
		inventory.addCoffee("0");
		int coffee = inventory.getCoffee();
		assertEquals(15, coffee);
	}
	
	public void testAddCoffee3(){
		Inventory inventory = new Inventory();
		try {
			inventory.addCoffee("-10");
		} catch (InventoryException e) {
			assertEquals("Units of coffee must be a positive integer", e.getMessage());
		}
	}
		
	public void testAddCoffee4(){
		Inventory inventory = new Inventory();
		try{
			inventory.addCoffee("salam");
		} catch(InventoryException e){
			assertEquals("Units of coffee must be a positive integer", e.getMessage());
		}
	}
	
	public void testAddMilk() throws InventoryException{
		Inventory inventory = new Inventory();
		inventory.addMilk("10");
		int milk = inventory.getMilk();
		assertEquals(25, milk);
	}
	
	public void testAddMilk2() throws InventoryException{
		Inventory inventory = new Inventory();
		inventory.addMilk("0");
		int milk = inventory.getMilk();
		assertEquals(15, milk);
	}
	
	public void testAddMilk3(){
		Inventory inventory = new Inventory();
		try {
			inventory.addMilk("salam");
		} catch (InventoryException e) {
			assertEquals("Units of milk must be a positive integer", e.getMessage());
		}
	}
	
	public void testAddMilk4(){
		Inventory inventory = new Inventory();
		try {
			inventory.addMilk("-10");
		} catch (InventoryException e) {
			assertEquals("Units of milk must be a positive integer", e.getMessage());
		}
	}
	
	public void testSugar1() throws InventoryException{
		Inventory inventory = new Inventory();
		inventory.addSugar("10");
		int sugar = inventory.getSugar();
		assertEquals(25, sugar);
	}
	
	public void testSugar2(){
		Inventory inventory = new Inventory();
		try {
			inventory.addSugar("-10");
		} catch (InventoryException e) {
			assertEquals("Units of sugar must be a positive integer", e.getMessage());
		}
	}
	
	public void testSugar3(){
		Inventory inventory = new Inventory();
		try {
			inventory.addSugar("salam");
		} catch (InventoryException e) {
			assertEquals("Units of sugar must be a positive integer", e.getMessage());
		}
	}
	
	
	public void testSetChocolate(){
		Inventory inventory = new Inventory();
		inventory.setChocolate(-10);
		int chocolate = inventory.getChocolate();
		assertEquals(15, chocolate);
	}
	
	public void testSetCoffee(){
		Inventory inventory = new Inventory();
		inventory.setCoffee(-10);
		int coffee = inventory.getCoffee();
		assertEquals(15, coffee);
	}
	
	public void testSetMilk(){
		Inventory inventory = new Inventory();
		inventory.setMilk(-10);
		int milk = inventory.getMilk();
		assertEquals(15, milk);
	}
	
	public void testSetSugar(){
		Inventory inventory = new Inventory();
		inventory.setSugar(-10);
		int sugar = inventory.getSugar();
		assertEquals(15, sugar);
	}
	
	public void testRecipe1(){
		Recipe r = new Recipe();
		Inventory inventory = new Inventory();
		assertEquals(true, inventory.useIngredients(r));
	}
	
	public void testRecipe2() throws RecipeException{
		Recipe r = new Recipe();
		r.setAmtChocolate("20");
		Inventory inventory = new Inventory();
		assertEquals(false, inventory.useIngredients(r));
	}
	
	public void testRecipe3() throws RecipeException{
		Recipe r = new Recipe();
		r.setAmtCoffee("10");
		
		Inventory inventory = new Inventory();
		inventory.useIngredients(r);
		
		Recipe r2 = new Recipe();
		r2.setAmtCoffee("10");
		
		assertEquals(false, inventory.useIngredients(r2));
		
	}
}