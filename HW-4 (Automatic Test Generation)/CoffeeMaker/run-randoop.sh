#!/bin/sh

ANT_HOME="lib/ant"

$ANT_HOME/bin/ant compile
java -javaagent:lib/exercised_agent-2.1.4.jar -classpath lib/randoop-2.1.4.jar:bin randoop.main.Main gentests \
    --classlist=coffeemaker.all \
    --include-if-class-exercised=coffeemaker.cover \
    --timelimit=60 \
    --no-error-revealing-tests=true \
    --junit-package-name=edu.ncsu.csc326.coffeemaker \
    --junit-output-dir=randoop-tests
